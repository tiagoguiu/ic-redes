import 'dart:convert';

class DataModel {
  String ip;
  String macDev;
  String routerIp;
  String frequency;
  String signal;
  String submask;
  String ssid;
  String ipv6;
  String bssid;
  String broadcast;

  DataModel({
    this.ip,
    this.macDev,
    this.routerIp,
    this.frequency,
    this.signal,
    this.submask,
    this.ssid,
    this.ipv6,
    this.bssid,
    this.broadcast,
  });

  Map toMap() => {
        'ip': ip,
        'macDev': macDev,
        'routerIp': routerIp,
        'frequency': frequency,
        'signal': signal,
        'submask': submask,
        'ssid': ssid,
        'ipv6': ipv6,
        'bssid': bssid,
        'broadcast': broadcast,
      };

  String toJson() => jsonEncode(toMap());
}
