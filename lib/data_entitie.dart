class DataEntity {
  final String ip;
  final String macDev;
  final String frequency;
  final String routerIp;
  final String signal;
  final String bssid;
  final String ssid;
  final String broadcast;
  final String submask;
  final String ipv6;

  DataEntity({
    this.ip,
    this.macDev,
    this.frequency,
    this.routerIp,
    this.signal,
    this.bssid,
    this.ssid,
    this.broadcast,
    this.submask,
    this.ipv6,
  });
}
