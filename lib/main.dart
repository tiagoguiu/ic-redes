import 'dart:io';

import 'package:flutter/material.dart';

import 'data_controller.dart';

import 'my_http_overrides.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    //instancia da aceitação do certificado ssl
    HttpOverrides.global = new MyHttpOverrides();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Iniciação cientifica'),
          centerTitle: true,
        ),
        body: DataController(),
      ),
    );
  }
}
