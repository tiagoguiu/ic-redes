

import 'dart:io';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

/*
Este trecho do codigo diz respeito ao android
quando a função badcertificatecallback retorna true
o cliente http do flutter pode aceitar conexões http/https
que não estejam 100% com seus certificados ou não sejam
totalmente validos
*/