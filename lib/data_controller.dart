import 'dart:convert';

import 'package:android_wifi_info/android_wifi_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:network_info_plus/network_info_plus.dart';
import 'package:wifi_info_plugin/wifi_info_plugin.dart';
import 'data_model.dart';

class DataController extends StatefulWidget {
  @override
  _DataControllerState createState() => _DataControllerState();
}

class _DataControllerState extends State<DataController> {
  WifiInfoWrapper _wifiObject;
  int error = 0;
  @override
  void initState() {
    initPlatformState();
    super.initState();
  }

  Future<void> initPlatformState() async {
    WifiInfoWrapper wifiObject;
    try {
      wifiObject = await WifiInfoPlugin.wifiDetails;
    } on PlatformException {}
    if (!mounted) return;

    setState(() {
      _wifiObject = wifiObject;
    });
  }

  Future<DataModel> setDados() async {
    final int sig = await AndroidWifiInfo.rssi;
    final info = NetworkInfo();
    return DataModel(
      ip: _wifiObject.ipAddress,
      macDev: _wifiObject.macAddress,
      routerIp: _wifiObject.routerIp,
      frequency: _wifiObject.frequency.toString(),
      signal: sig.toString(),
      broadcast: await info.getWifiBroadcast(),
      bssid: await info.getWifiBSSID(),
      ssid: await info.getWifiName(),
      ipv6: await info.getWifiIPv6(),
      submask: await info.getWifiSubmask(),
    );
  }

  // ignore: missing_return
  Future<Icon> getFutureDados() async {
    try {
      DataModel info = await setDados();
      await Future.delayed(Duration(seconds: 3));
      final http.Response response = await http
          .post("https://apirestiniciacao.herokuapp.com/api/posts",
              headers: <String, String>{
                'Content-Type': 'application/json; charset=UTF-8'
              },
              body: jsonEncode({
                'ip': info.ip,
                'macdev': info.macDev,
                'routerIp': info.routerIp,
                'frequency': info.frequency,
                'signal': info.signal,
                'submask': info.submask,
                'ssid': info.ssid,
                'ipv6': info.ipv6,
                'bssid': info.bssid,
                'broadcast': info.broadcast,
              }))
          .timeout(Duration(seconds: 10));
      print(info.toJson());
      if (response.statusCode == 200) {
        Map<String, dynamic> json = jsonDecode(response.body);
        print(json);
        error = 1;
        return Icon(Icons.check_box);
      } else {
        error = 2;
      }
    } catch (e) {
      error = 2;
      print(e.toString());

      return Icon(Icons.error);
    }
  }

  buildContainer() {
    return Container(
      child: FutureBuilder(
          future: getFutureDados(),
          builder: (context, snapshot) {
            if (error == 1) {
              return Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Enviado"),
                    snapshot.data,
                  ],
                ),
              );
            } else if (error == 2) {
              return Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Não foi possivel enviar"),
                    Icon(Icons.error),
                  ],
                ),
              );
            } else {
              return Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('ATIVE SUA LOCALIZAÇÃO(GPS)'),
                    const SizedBox(height: 20),
                    CircularProgressIndicator(),
                    const SizedBox(height: 20),
                    Text("Enviando dados..."),
                    const SizedBox(height: 10),
                  ],
                ),
              );
            }
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return buildContainer();
  }
}
