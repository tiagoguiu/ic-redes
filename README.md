># APLICAÇÃO DE INICIAÇÃO CIENTIFICA PARA PROVEDORES DE INTERNET

![Badge](https://img.shields.io/badge/FLUTTER-MOBILE-blue)
![Badge](https://img.shields.io/badge/DART-GOOGLE-orange)
![Badge](https://img.shields.io/badge/MONGODB-DATABASE-red)
![Badge](https://img.shields.io/badge/NETWORK-MONITOR-green)
>## UMA BREVE INTRODUÇÃO:
+ Com uma ideia vinda do professor do departamento de computação fernando teixeira, de fazer a coleta de informações sobre a rede interna do usuario e levar até o administrador da rede com o intuito de ter um atendimento mais direto ao usuario final e um status personalizado de como estaria cada rede.

>## DESENVOLVIMENTO FULL-STACK DA APLICAÇÃO UTILIZANDO AS SEGUINTES TECNOLOGIAS:

+ FLUTTER
+ DART LANGUAGE
+ MONGODB
+ WIFI INFO PLUGIN E ANDROID WIFI INFO 
+ HTTPS/SSL
+ NODE.JS(BACK-END)

>### PRÓXIMAS ADIÇÕES:
+ Rodar o app em backgrond
+ Coletar mais informações
+ Timer de coleta



